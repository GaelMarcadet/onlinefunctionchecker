# Commandes
commandes = {
    # Le client envoie au serveur une demande de test
    "TEST": {
        "commande": "TEST",
        "nom": None
    },
    # Le serveur renvoie au client la liste des valeurs à tester
    "VALEURS": {
        "commande": "VALEURS",
        "nom": None,
        "valeurs": []
    },
    # Le client renvoie au serveur les valeurs rendues par sa fonction
    "VERIF": {
        "commande": "VERIF",
        "nom": None,
        "resultats": []
    },
    # Le serveur renvoie la réponse si la fonction a passé les tests ou non
    "RESULT": {
        "commande": "RESULT",
        "nom": None,
        "reussi": 0,
        "note": "Aucune précision"
    },
    # Le serveur demande une lever d'exception chez le client
    "EXCEPT": {
        "commande": "EXCEPT",
        "nom": None,
        "raison": "Indéfini"
    }
}

def type_commande(requete):
    return requete["commande"]

def nom_fonction(requete):
    return requete["nom"]
