#! /usr/bin/python3

from socket import socket
from commandes import *
from threading import Thread
import json
import random
from socket import socket, SOL_SOCKET, SO_REUSEADDR



port = 4445

s = socket()
s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
s.bind(('0.0.0.0', port))
s.listen(1)

def generation_valeurs(series = 30, valeurs = 1,  min = 0, max = 100):
    if max <= min:
        aux = min
        min = max
        max = aux
    if valeurs == 1:
        return [random.randint(min,max) for _ in range(series)]
    else:
        return [[random.randint(min,max) for _ in range(valeurs)] for _ in range(series)]

def exception_client(client, nom_fonction, raison):
    req = commandes["EXCEPT"].copy()
    req["nom"] = nom_fonction
    req["raison"] = raison
    client.send(json.dumps(req, separators = (',', ':')).encode())
    client.close()

def envoyer_resultats_test(client, nom_fonction, reussi, note = None):
    req = commandes["RESULT"].copy()
    req["nom"] = nom_fonction
    req["reussi"] = reussi
    if note:
        req["note"] = note

    client.send(json.dumps(req, separators = (',', ':')).encode())

    client.close()


#################################
#       Définition classes      #
#################################

def envoyer_et_attendre_reponse(socket_client, requete):
    socket_client.send(json.dumps(requete, separators=(',',':')).encode())
    return json.loads(socket_client.recv(2048).decode())


class TestFonctionServeur:
    def __init__(self, nom_fonction, fonction):
        self.nom_fonction = nom_fonction
        self.fonction = fonction

    def run(self, socket_client):
        requete_valeurs = commandes["VALEURS"].copy()
        requete_valeurs["nom"] = self.nom_fonction

        requete_valeurs["valeurs"] = self.generation_valeurs()

        print("[LOG] Valeurs envoyées au client: ", requete_valeurs["valeurs"])

        requete_resultats = envoyer_et_attendre_reponse(socket_client, requete_valeurs)
        print("[LOG] Résultats reçus du client: ", requete_resultats["resultats"])
        (reussi, note) = self.verification(requete_valeurs["valeurs"], requete_resultats["resultats"])
        if reussi:
            print("[LOG] Test réussi avec succès")
            envoyer_resultats_test(socket_client, self.nom_fonction, 1)
        else:
            print("[LOG] Test échoué")
            envoyer_resultats_test(socket_client, self.nom_fonction, 0, note)

    def get_nom_fonction(self):
        return self.nom_fonction

    def generation_valeurs(self):
        pass

    def verification(self, valeurs, resultats):
        index = 0
        liste_valeurs_erreurs = []
        for x in valeurs:
            if not self.traitement_reponse_client(x, resultats[index]):
                liste_valeurs_erreurs.append(str(x))
            index = index + 1
        if liste_valeurs_erreurs == []:
            return (1,"Bravo")
        else:
            if len(liste_valeurs_erreurs) == 1:
                return (0, "Erreur avec la valeur " + liste_valeurs_erreurs[0])
            else:
                return (0, "Erreur avec les valeurs " + str(liste_valeurs_erreurs))

    def traitement_reponse_client(self, valeur, resultat_client):
        return self.fonction(valeur) == resultat_client

class TestFonctionArite1(TestFonctionServeur):
    def __init__(self, nom_fonction, fonction):
        TestFonctionServeur.__init__(self, nom_fonction, fonction)

    def generation_valeurs(self):
        return generation_valeurs(valeurs = 1)

class TestFonctionArite2(TestFonctionServeur):
    def __init__(self, nom_fonction, fonction):
        TestFonctionServeur.__init__(self, nom_fonction, fonction)

    def generation_valeurs(self):
        return generation_valeurs(valeurs = 2)

class TestFonctionAriteN(TestFonctionServeur):
    def __init__(self, nom_fonction, fonction):
        TestFonctionServeur.__init__(self, nom_fonction, fonction)

    def generation_valeurs(self):
        return generation_valeurs(valeurs = 30)


########################################################

class TestIdentiteServeur(TestFonctionArite1):
    def __init__(self):
        TestFonctionArite1.__init__(self, "identite", lambda x: x)

class TestSuccesseurServeur(TestFonctionArite1):
    def __init__(self):
        TestFonctionArite1.__init__(self, "successeur", lambda x: x + 1)

class TestCarreServeur(TestFonctionArite1):
    def __init__(self):
        TestFonctionArite1.__init__(self, "carre", lambda x: x *x)

class TestPairServeur(TestFonctionArite1):
    def __init__(self):
        TestFonctionArite1.__init__(self, "pair", lambda x: x % 2 == 0)

class TestCoupleServeur(TestFonctionArite2):
    def __init__(self):
        def addition(l):
            return l[0] + l[1]
        TestFonctionArite2.__init__(self,"addition", addition)


class TestDivisionServeur(TestFonctionArite2):
    def __init__(self):
        def division(l):
            if l[1] == 0:
                return None
            else:
                return (l[0]/l[1])
        TestFonctionArite2.__init__(self,"division", division)






##################################

liste_fonctions = []

liste_fonctions.append(TestIdentiteServeur())
liste_fonctions.append(TestSuccesseurServeur())
liste_fonctions.append(TestCarreServeur())
liste_fonctions.append(TestPairServeur())
liste_fonctions.append(TestCoupleServeur())
liste_fonctions.append(TestDivisionServeur())




class ThreadClient(Thread):
    def __init__(self, socket_client):
        Thread.__init__(self)
        self.socket_client = socket_client

    def run(self):
        print("-------------  Server  -------------")
        print("[LOG] Nouveau client connecté: ", addr)
        req = json.loads(client.recv(2048).decode())
        print(req)
        type = type_commande(req)
        print("[LOG] Commande reçu du client: ", type)

        if type == "TEST":
            fonction_connue = 0
            nom = nom_fonction(req)
            print("[LOG] Test demandé sur la fonction ", nom)
            for fonction in liste_fonctions:
                if nom == fonction.get_nom_fonction():
                    fonction_connue = 1
                    print("[LOG] Fonction reconnue: début des tests")
                    fonction.run(client)
            if not fonction_connue:
                print("[LOG] Fonction non reconnue: envoie exception")
                exception_client(client, req["nom"], "Fonction non reconnue")

        else:
            print("[FATAL ERROR] Commande non reconnue: ", type)
            exception_client(client, type, "Type de requête non reconnue")


while 1:
    try:
        (client, addr) = s.accept()
        ThreadClient(client).start()
    except Exception as e:
        print("[FIN DE COMMUNICATION] Le client a mis fin prématurement à la connexion: " + str(e))
