from socket import socket
from commandes import *
import json

port = 4445

def connexion():
    s = socket()
    try:
        s.connect(('localhost', port))
        print("[LOG] Connexion avec le serveur établie")
        return s
    except:
        print("[ERREUR SYSTEME] La connexion avec le serveur n'a pu s'établir, veuillez contacter un responsable")
        exit(1)

def attente_reponse(serveur):
    reponse = json.loads(serveur.recv(2048).decode())
    if type_commande(reponse) == "EXCEPT":
        print("[ERREUR]: ", reponse["nom"],": ", reponse["raison"])
        serveur.close()
        exit(1)
    return reponse

def envoyer_et_attendre_reponse(socket_serveur, requete):
    try:
        socket_serveur.send(json.dumps(requete, separators=(',',':')).encode())
        return attente_reponse(socket_serveur)
    except Exception as e:
        print("[ERREUR][SERVEUR] Le serveur a mis fin prématurement à la connexion")


class TestFonction:
    def __init__(self, nom_fonction, fonction):
        self.nom_fonction = nom_fonction
        self.fonction = fonction
        self.resultats = []
        self.run()

    def calculer_resultats(self,fonction, valeurs):
        for x in valeurs:
            self.resultats.append(fonction(x))

    def run(self):
        print("-------  Test[",self.nom_fonction,"]  -------")
        serveur = connexion()
        requete_test = commandes["TEST"].copy()
        requete_test["nom"] = self.nom_fonction

        # Envoie demande de test et reception des valeurs
        #print("[LOG] Préparation et envoie de la requête de test: ", requete_test)
        retour_valeurs = envoyer_et_attendre_reponse(serveur,requete_test)
        #print("[LOG] Réception des valeurs à tester: ", retour_valeurs)
        print("[LOG] Réception des valeurs à tester")
        valeurs = retour_valeurs["valeurs"]
        try:
            self.calculer_resultats(self.fonction, valeurs)
        except Exception as e:
            print("[ERREUR CALCUL] Une erreur est survenue lors du calcul des valeurs: " + str(e))
            serveur.close()
            exit(1)

        print("[LOG] Calcul des valeurs du serveur terminé")
        #print("[LOG] Calcul des tests effectués: ", self.resultats)

        requete_verification = commandes["VERIF"].copy()
        requete_verification["nom"] = self.nom_fonction
        requete_verification["resultats"] = self.resultats

        #print("[LOG] Préparation et envoie de la requête de vérification: ", requete_verification)
        print("[LOG] Envoie de la requête de vérification")
        resultat = envoyer_et_attendre_reponse(serveur, requete_verification)
        print("[LOG] Vérification reçue")
        if resultat['reussi']:
            print("[RESULTAT][Validé] La fonction ", self.nom_fonction, " a été validée par le serveur")
        else:
            print("[RESULTAT][Echoué] La fonction ", self.nom_fonction, " contient une erreur: ", resultat["note"])
        serveur.close()


class test_identite(TestFonction):
    def __init__(self, fonction):
        TestFonction.__init__(self, "identite", fonction)

class test_successeur(TestFonction):
    def __init__(self, fonction):
        TestFonction.__init__(self, "successeur", fonction)

class test_carre(TestFonction):
    def __init__(self, fonction):
        TestFonction.__init__(self, "carre", fonction)

class test_pair(TestFonction):
    def __init__(self, fonction):
        TestFonction.__init__(self, "pair", fonction)

class test_addition(TestFonction):
    def __init__(self, fonction):
        TestFonction.__init__(self, "addition", fonction)

class test_division(TestFonction):
    def __init__(self, fonction):
        TestFonction.__init__(self, "division", fonction)
